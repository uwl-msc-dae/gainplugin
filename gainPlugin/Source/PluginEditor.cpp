
#include "PluginProcessor.h"
#include "PluginEditor.h"

GainPluginAudioProcessorEditor::GainPluginAudioProcessorEditor (GainPluginAudioProcessor& p)
    : AudioProcessorEditor (&p), audioProcessor (p)
{
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.
    setSize (400, 300);
    
    gainSlider.setSliderStyle(Slider::LinearBarVertical);
    gainSlider.setRange(-48.0f, 60.0f, 0.1f);
    
    gainSlider.setTextBoxStyle(Slider::NoTextBox, false, 90, 0);
    gainSlider.setPopupDisplayEnabled(true, false, this);
    gainSlider.setTextValueSuffix(" dB");
    
    gainSlider.setValue(1.0f);
    
    gainSlider.addListener(this);
    addAndMakeVisible(gainSlider);
    
}

GainPluginAudioProcessorEditor::~GainPluginAudioProcessorEditor()
{
}

void GainPluginAudioProcessorEditor::sliderValueChanged(Slider *slider)
{
    if (slider == &gainSlider)
    {
        audioProcessor.gainDB = gainSlider.getValue();
    }
}

void GainPluginAudioProcessorEditor::paint (juce::Graphics& g)
{
    g.fillAll (Colours::black);
    g.setColour(Colours::white);
    g.drawFittedText("GAIN", 0, 0, getWidth(), getHeight(), Justification::centred, 1);
}

void GainPluginAudioProcessorEditor::resized()
{
    gainSlider.setBounds(40, 30, 20, getHeight() - 60);
}
