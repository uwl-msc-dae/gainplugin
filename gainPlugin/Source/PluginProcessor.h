
#pragma once

#include <JuceHeader.h>

#define LEFT 0
#define RIGHT 1
#define CENTRE 2
#define LEFTSURR 3
#define RIGHTSURR 4
#define SUBLFE 5

using namespace juce;
using namespace std;

class GainPluginAudioProcessor  : public juce::AudioProcessor
{
public:
    GainPluginAudioProcessor();
    ~GainPluginAudioProcessor() override;
    
    void prepareToPlay (double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

   #ifndef JucePlugin_PreferredChannelConfigurations
    bool isBusesLayoutSupported (const BusesLayout& layouts) const override;
   #endif

    void processBlock (juce::AudioBuffer<float>&, juce::MidiBuffer&) override;
    
    juce::AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;
    
    const juce::String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    bool isMidiEffect() const override;
    double getTailLengthSeconds() const override;

    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram (int index) override;
    const juce::String getProgramName (int index) override;
    void changeProgramName (int index, const juce::String& newName) override;

    void getStateInformation (juce::MemoryBlock& destData) override;
    void setStateInformation (const void* data, int sizeInBytes) override;
    
    float gainDB = 1;

private:
    //float gainRamp = 0.5;
    float channelDataPrev = 0;
    float alpha = 0.99;
    float gainRampOutput = 0;
    float gainLinearPrev = 0;
    //vector<vector<float>> channelDataPrevVctr;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (GainPluginAudioProcessor)
};
